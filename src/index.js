import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "../src/Components/App/App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import reducer from "../src/Components/Redux/reducer";
import rootSaga from "../src/Components/Redux/Sagas/index";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { BrowserRouter } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <App />
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
