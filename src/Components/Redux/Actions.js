import {
  FETCH_LOGIN,
  FETCH_REGISTRATION,
  FETCH_SIGNOUT,
  FETCH_RESETPASSWORD,
  FETCH_UPDATEPASSWORD,
} from "./Types";

export function login(
  email,
  password,
  history,
  loginCallback,
  setStatePreloader
) {
  return {
    type: FETCH_LOGIN,
    email: email,
    password: password,
    history: history,
    loginCallback: loginCallback,
    setStatePreloader: setStatePreloader,
  };
}

export function registration(
  email,
  password,
  history,
  loginCallback,
  setStatePreloader
) {
  return {
    type: FETCH_REGISTRATION,
    email: email,
    password: password,
    history: history,
    loginCallback: loginCallback,
    setStatePreloader: setStatePreloader,
  };
}

export function signOut(history, setStatePreloader) {
  return {
    type: FETCH_SIGNOUT,
    history: history,
    setStatePreloader: setStatePreloader,
  };
}

export function resetPassword(email, setStatePreloader) {
  return {
    type: FETCH_RESETPASSWORD,
    email: email,
    setStatePreloader: setStatePreloader,
  };
}

export function updatePassword(password, history, setStatePreloader) {
  return {
    type: FETCH_UPDATEPASSWORD,
    password: password,
    history: history,
    setStatePreloader: setStatePreloader,
  };
}
