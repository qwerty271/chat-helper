import { call, takeLatest } from "redux-saga/effects";
import { FETCH_SIGNOUT } from "../Types.js";
import { appInit } from "../Firebase";
import { toast } from "react-toastify";

function* fetchSignOut(action) {
  action.setStatePreloader(true);
  const data = yield call(async () => {
    try {
      await appInit.auth().signOut();
      toast.success("Вы вышли из аккаунта");
      localStorage.removeItem("token");
      action.setStatePreloader(false);
      action.history.push("/login");
    } catch (error) {
      action.setStatePreloader(false);
      toast.error("Ошибка");
      console.log(error);
    }
  });
  yield data;
}

export default function* signOutSaga() {
  yield takeLatest(FETCH_SIGNOUT, fetchSignOut);
}
