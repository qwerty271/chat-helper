import { call, takeLatest } from "redux-saga/effects";
import { FETCH_REGISTRATION } from "../Types.js";
import { appInit } from "../Firebase";
import { toast } from "react-toastify";

function* fetchRegistration(action) {
  action.setStatePreloader(true);
  const data = yield call(async () => {
    try {
      const success = await appInit
        .auth()
        .createUserWithEmailAndPassword(action.email, action.password);
      localStorage.setItem("token", success.user._delegate.accessToken);
      toast.success("Вы успешно зарегистрировались");
      action.loginCallback();
      action.setStatePreloader(false);
      action.history.push("/");
    } catch (error) {
      action.setStatePreloader(false);
      toast.error("Ошибка регистрации");
      console.log(error);
    }
  });
  yield data;
}

export default function* registrationSaga() {
  yield takeLatest(FETCH_REGISTRATION, fetchRegistration);
}
