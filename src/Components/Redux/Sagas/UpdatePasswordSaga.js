import { call, takeLatest } from "redux-saga/effects";
import { FETCH_UPDATEPASSWORD } from "../Types.js";
import { appInit } from "../Firebase";
import { toast } from "react-toastify";

function* fetchUpdatePassword(action) {
  action.setStatePreloader(true);
  const url = window.location.href;
  const oobCode = url
    .substring(url.indexOf("oobCode="), url.indexOf("&apiKey"))
    .replace(/.*oobCode=/, "");
  const data = yield call(async () => {
    try {
      await appInit.auth().confirmPasswordReset(oobCode, action.password);
      toast.success("Пароль успешно обновлен");
      action.setStatePreloader(false);
      action.history.push("/login");
    } catch (error) {
      action.setStatePreloader(false);
      toast.error("Ошибка");
      console.log(error);
    }
  });
  yield data;
}

export default function* updatePasswordSaga() {
  yield takeLatest(FETCH_UPDATEPASSWORD, fetchUpdatePassword);
}
