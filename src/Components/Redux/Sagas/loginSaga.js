import { call, takeLatest } from "redux-saga/effects";
import { FETCH_LOGIN } from "../Types.js";
import { appInit } from "../Firebase";
import { toast } from "react-toastify";

function* fetchLogin(action) {
  action.setStatePreloader(true);
  const data = yield call(async () => {
    try {
      const success = await appInit
        .auth()
        .signInWithEmailAndPassword(action.email, action.password);
      localStorage.setItem("token", success.user._delegate.accessToken);
      action.loginCallback();
      action.setStatePreloader(false);
      action.history.push("/");
      toast.success("Вы успешно авторизовались");
    } catch (error) {
      action.setStatePreloader(false);
      toast.error("Ошибка авторизации");
      console.log(error);
    }
  });
  yield data;
}

export default function* loginSaga() {
  yield takeLatest(FETCH_LOGIN, fetchLogin);
}
