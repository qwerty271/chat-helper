import { call, takeLatest } from "redux-saga/effects";
import { FETCH_RESETPASSWORD } from "../Types.js";
import { appInit } from "../Firebase";
import { toast } from "react-toastify";

function* fetchResetPassword(action) {
  action.setStatePreloader(true);
  const data = yield call(async () => {
    try {
      await appInit.auth().sendPasswordResetEmail(action.email);
      action.setStatePreloader(false);
      toast.success("Ссылка для восстановления пароля отправлена на почту");
    } catch (error) {
      action.setStatePreloader(false);
      toast.error("Ошибка");
      console.log(error);
    }
  });
  yield data;
}

export default function* resetPasswordSaga() {
  yield takeLatest(FETCH_RESETPASSWORD, fetchResetPassword);
}
