import { all } from "redux-saga/effects";
import loginSaga from "./loginSaga";
import registrationSaga from "./RegistrationSaga";
import resetPasswordSaga from "./ResetPasswordSaga";
import updatePasswordSaga from "./UpdatePasswordSaga";
import signOutSaga from "./SignOutSaga";

export default function* rootSaga() {
  yield all([
    loginSaga(),
    registrationSaga(),
    resetPasswordSaga(),
    updatePasswordSaga(),
    signOutSaga(),
  ]);
}
