import firebase from "firebase/compat/app";
import "firebase/compat/auth";

export const appInit = firebase.initializeApp({
  apiKey: "AIzaSyD2zPJAmdKb_4BufBMzQLXTnGjkkFhN_Vg",
  authDomain: "chat-helper-e94fa.firebaseapp.com",
  projectId: "chat-helper-e94fa",
  storageBucket: "chat-helper-e94fa.appspot.com",
  messagingSenderId: "33147802364",
  appId: "1:33147802364:web:b3d6f799080ed7876f7d79",
  measurementId: "G-5TKYSEK2KX",
});
