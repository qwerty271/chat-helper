const initialState = {
  user: {},
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "":
      return {
        user: action.data,
      };
    default:
      return state;
  }
}
