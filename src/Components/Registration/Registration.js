import React from "react";
import "./Registration.css";
import Form from "../Form/Form";

function Registration(props) {
  return (
    <section className="registration">
      <Form
        title={"Добро пожаловать!"}
        button={"Зарегистрироваться"}
        route={"registration"}
        submit={props.registration}
      />
    </section>
  );
}

export default Registration;
