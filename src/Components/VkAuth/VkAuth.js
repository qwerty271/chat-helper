import Preloader from "../Preloader/Preloader";
import { useHistory } from "react-router-dom";

function VkAuth(props) {
  const history = useHistory();
  const url = window.location.href;
  const token = url
    .substring(url.indexOf("access_token="), url.indexOf("&expires_in"))
    .replace(/.*access_token=/, "");
  const userEmail = url.replace(/.*email=/, "");
  if (token) {
    localStorage.setItem("token", token);
    props.setCurrentUser({ email: userEmail });
    props.loginCallback();
    history.push("/");
  } else {
    history.push("/login");
  }

  return <Preloader state={true} />;
}

export default VkAuth;
