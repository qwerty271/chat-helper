import React from "react";
import "./ForgotPassword.css";
import Form from "../Form/Form";

function ForgotPassword(props) {
  return (
    <section className="forgotPassword">
      <Form
        title={"Забыли пароль?"}
        button={"Отправить ссылку для восстановления"}
        route={"forgotPassword"}
        submit={props.forgotPassword}
      />
    </section>
  );
}

export default ForgotPassword;
