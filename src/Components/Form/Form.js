import React from "react";
import "./Form.css";
import { Formik } from "formik";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import vk_logo from "../../images/vk-logo.png";
import google_logo from "../../images/google-logo.png";
import { appInit } from "../Redux/Firebase";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import { Input } from "reactstrap";

function Form(props) {
  const formTitle = props.title;
  const fieldsButton = props.button;
  const route = props.route;
  const history = useHistory();

  function googleAuth() {
    const provider = new firebase.auth.GoogleAuthProvider();
    appInit
      .auth()
      .signInWithPopup(provider)
      .then((result) => {
        if (result) {
          const token = result.credential.accessToken;
          localStorage.setItem("token", token);
          props.loginCallback();
          history.push("/");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <section className="form">
      <h2 className="form__title">{formTitle}</h2>

      <Formik
        initialValues={{ email: "", password: "", confirmPassword: "" }}
        validate={(values) => {
          const errors = {};
          if (
            route === "registration" ||
            route === "login" ||
            route === "forgotPassword"
          ) {
            if (!values.email) {
              errors.email = "Это обязательное поле";
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = "Неверный формат почты";
            }
          }

          if (route === "registration" || route === "updatePassword") {
            if (!values.confirmPassword) {
              errors.confirmPassword = "Это обязательное поле";
            } else if (values.confirmPassword.length < 8) {
              errors.confirmPassword =
                "Пароль должен иметь длину не менее 8 знаков";
            } else if (
              !/[A-Z]/g.test(values.confirmPassword) ||
              !/[a-z]/g.test(values.confirmPassword) ||
              !/[0-9]/g.test(values.confirmPassword)
            ) {
              errors.confirmPassword =
                "Пароль должен содержать цифру, буквы в нижнем и верхнем регистре";
            } else if (values.confirmPassword !== values.password) {
              errors.confirmPassword = "Пароли не совпадают";
            }
          }
          if (
            route === "registration" ||
            route === "login" ||
            route === "updatePassword"
          ) {
            if (!values.password) {
              errors.password = "Это обязательное поле";
            } else if (values.password.length < 8) {
              errors.password = "Пароль должен иметь длину не менее 8 знаков";
            } else if (
              !/[A-Z]/g.test(values.password) ||
              !/[a-z]/g.test(values.password) ||
              !/[0-9]/g.test(values.password)
            ) {
              errors.password =
                "Пароль должен содержать цифру, буквы в нижнем и верхнем регистре";
            }
          }

          return errors;
        }}
        onSubmit={(values) => {
          switch (route) {
            case "registration":
              return props.submit(
                values.email,
                values.password,
                values.confirmPassword
              );
            case "login":
              return props.submit(values.email, values.password);
            case "forgotPassword":
              return props.submit(values.email);
            case "updatePassword":
              return props.submit(values.password);
            default:
              return null;
          }
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          status,
          isSubmitting,
        }) => (
          <form className="fields" onSubmit={handleSubmit}>
            <p
              className={`fields__title ${
                route === "updatePassword" ? "fields__title_inactive" : ""
              }`}
            >
              E-mail
            </p>

            <Input
              bsize="lg"
              type={`${route === "updatePassword" ? "hidden" : "email"}`}
              invalid={
                errors.email && touched.email && errors.email ? true : false
              }
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              placeholder="E-mail"
            />
            <span className="fields__span-error">
              {errors.email && touched.email && errors.email}
            </span>

            <p
              className={`fields__title ${
                route === "forgotPassword" ? "fields__title_inactive" : ""
              }`}
            >
              Пароль
            </p>

            <Input
              bsize="lg"
              type={`${route === "forgotPassword" ? "hidden" : "password"}`}
              invalid={
                errors.password && touched.password && errors.password
                  ? true
                  : false
              }
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              placeholder="Пароль"
            />
            <span className="fields__span-error">
              {errors.password && touched.password && errors.password
                ? errors.password && touched.password && errors.password
                : status
                ? status
                : ""}
            </span>

            <p
              className={`fields__title ${
                route === "forgotPassword" || route === "login"
                  ? "fields__title_inactive"
                  : ""
              }`}
            >
              Подтверждение пароля
            </p>

            <Input
              bsize="lg"
              type={`${
                route === "forgotPassword" || route === "login"
                  ? "hidden"
                  : "password"
              }`}
              invalid={
                errors.confirmPassword &&
                touched.confirmPassword &&
                errors.confirmPassword
                  ? true
                  : false
              }
              name="confirmPassword"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.confirmPassword}
              placeholder="Подтверждение пароля"
            />
            <span className="fields__span-error">
              {errors.confirmPassword &&
                touched.confirmPassword &&
                errors.confirmPassword}
            </span>

            <button
              className={`fields__button  ${
                (errors.confirmPassword &&
                  touched.confirmPassword &&
                  errors.confirmPassword) ||
                (errors.email && touched.email && errors.email) ||
                (errors.password && touched.password && errors.password)
                  ? "fields__button_inactive"
                  : ""
              } ${
                route === "login"
                  ? touched.email && touched.password
                    ? ""
                    : "fields__button_inactive"
                  : ""
              } ${
                route === "registration"
                  ? touched.email && touched.password && touched.confirmPassword
                    ? ""
                    : "fields__button_inactive"
                  : ""
              }
              ${
                route === "forgotPassword"
                  ? touched.email
                    ? ""
                    : "fields__button_inactive"
                  : ""
              }
              ${
                route === "updatePassword"
                  ? touched.password && touched.confirmPassword
                    ? ""
                    : "fields__button_inactive"
                  : ""
              }`}
              type="submit"
              disabled={`${
                (errors.confirmPassword &&
                  touched.confirmPassword &&
                  errors.confirmPassword) ||
                (errors.email && touched.email && errors.email) ||
                (errors.password && touched.password && errors.password)
                  ? "disabled"
                  : ""
              }`}
            >
              {fieldsButton}
            </button>
          </form>
        )}
      </Formik>
      <ul
        className={`${
          route === "login" ? "login-list" : "login-list_inactive"
        }`}
      >
        <li className="login-list__item">
          <a
            className="login-list__link"
            href="https://oauth.vk.com/authorize?client_id=7955246&display=popup&redirect_uri=https://chat-helper.vercel.app/vkAuth&scope=email&response_type=token"
            target="_blank"
            rel="noreferrer"
          >
            Войти через Vk
          </a>
          <img className="login-list__logo" src={vk_logo} alt="Логотип Vk" />
        </li>
        <li className="login-list__item">
          <p className="login-list__link" onClick={googleAuth}>
            Войти через Google
          </p>
          <img
            className="login-list__logo"
            src={google_logo}
            alt="Логотип Google"
          />
        </li>
      </ul>
      <ul className="form__list">
        <li className="form__item">
          <Link
            to={`/${route === "login" ? "registration" : "login"}`}
            className="form__link"
          >
            {route === "login" ? "Зарегистрироваться" : "Войти"}
          </Link>
        </li>
        <li className="form__item">
          <Link
            to={`/${
              route === "registration" || route === "login"
                ? "forgotPassword"
                : "registration"
            }`}
            className="form__link"
          >
            {route === "registration" || route === "login"
              ? "Забыли пароль?"
              : "Зарегистрироваться"}
          </Link>
        </li>
      </ul>
    </section>
  );
}

function mapStateToProps(state) {
  return { data: state };
}

export default connect(mapStateToProps)(Form);
