import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { shallow } from "enzyme";
import Login from "./Login";
import Form from "../Form/Form";

configure({ adapter: new Adapter() });

describe("<Login />", () => {
  it("Компонент <Form/> отрисовался", () => {
    const MyComponent = shallow(<Login />);
    expect(MyComponent.find(Form)).toHaveLength(1);
  });
});
