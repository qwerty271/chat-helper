import React from "react";
import "./Login.css";
import Form from "../Form/Form";

function Login(props) {
  return (
    <section className="login">
      <Form
        title={"Пожалуйста, авторизуйтесь"}
        button={"Войти"}
        route={"login"}
        submit={props.login}
        loginCallback={props.loginCallback}
      />
    </section>
  );
}

export default Login;
