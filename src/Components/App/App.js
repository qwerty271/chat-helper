import "./App.css";
import React from "react";
import Login from "../Login/Login";
import Main from "../Main/Main";
import {
  login,
  registration,
  resetPassword,
  updatePassword,
  signOut,
} from "../Redux/Actions";
import { useDispatch, connect } from "react-redux";
import Registration from "../Registration/Registration";
import ForgotPassword from "../ForgotPassword/ForgotPassword";
import { Route, Switch, useHistory } from "react-router-dom";
import UpdatePassword from "../UpdatePassword/UpdatePassword";
import { ToastContainer } from "react-toastify";
import ProtectedRoute from "../ProtectedRoute/ProtectedRoute";
import "react-toastify/dist/ReactToastify.css";
import { CurrentUserContext } from "../../contexts/CurrentUserContext";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import Preloader from "../Preloader/Preloader";
import VkAuth from "../VkAuth/VkAuth";

function App() {
  const dispatch = useDispatch();
  const [loggedIn, setLoggedIn] = React.useState(true);
  const [currentUser, setCurrentUser] = React.useState({});
  const [statePreloader, setStatePreloader] = React.useState(false);
  const history = useHistory();
  const auth = getAuth();
  const loginCallback = React.useCallback(() => {
    onAuthStateChanged(auth, (user) => {
      setCurrentUser(user);
    });
    setLoggedIn(true);
  }, [auth]);

  React.useEffect(() => {
    const token = localStorage.getItem("token");

    if (token) {
      loginCallback();
    } else if (!token) {
      setLoggedIn(false);
    }
  }, [loginCallback]);
  console.log(loggedIn);
  console.log(currentUser);

  function userLogin(email, password) {
    dispatch(login(email, password, history, loginCallback, setStatePreloader));
  }

  function userRegistration(email, password) {
    dispatch(
      registration(email, password, history, loginCallback, setStatePreloader)
    );
  }

  function userForgotPassword(email) {
    dispatch(resetPassword(email, setStatePreloader));
  }

  function userUpdatePassword(password) {
    dispatch(updatePassword(password, history, setStatePreloader));
  }

  function userSignOut() {
    dispatch(signOut(history, setStatePreloader));
  }

  return (
    <CurrentUserContext.Provider value={currentUser}>
      <div className="page">
        <Switch>
          <Route exact path="/login">
            <Login login={userLogin} loginCallback={loginCallback} />
          </Route>
          <Route path="/registration">
            <Registration registration={userRegistration} />
          </Route>
          <Route path="/forgotPassword">
            <ForgotPassword forgotPassword={userForgotPassword} />
          </Route>
          <Route path="/updatePassword">
            <UpdatePassword updatePassword={userUpdatePassword} />
          </Route>
          <Route path="/vkAuth">
            <VkAuth
              loginCallback={loginCallback}
              setCurrentUser={setCurrentUser}
            />
          </Route>
          <ProtectedRoute
            path="/"
            type={loggedIn}
            component={Main}
            signOut={userSignOut}
            route={"main"}
          />
        </Switch>
        <ToastContainer
          theme="colored"
          position="top-center"
          autoClose="2500"
        />

        <Preloader state={statePreloader} />
      </div>
    </CurrentUserContext.Provider>
  );
}

function mapStateToProps(state) {
  return { data: state };
}

export default connect(mapStateToProps)(App);
