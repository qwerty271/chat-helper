import React from "react";
import "./UpdatePassword.css";
import Form from "../Form/Form";

function UpdatePassword(props) {
  return (
    <section className="updatePassword">
      <Form
        title={"Обновить пароль"}
        button={"Обновить"}
        route={"updatePassword"}
        submit={props.updatePassword}
      />
    </section>
  );
}

export default UpdatePassword;
