import "./Main.css";
import React from "react";
import chat_active_icon_white from "../../images/chat_active_icon_white.svg";
import chat_inactive_icon_white from "../../images/chat_inactive_icon_white.svg";
import chat_save_icon_white from "../../images/chat_save_icon_white.svg";
import { useHistory } from "react-router-dom";
import { CurrentUserContext } from "../../contexts/CurrentUserContext";

function Main(props) {
  const history = useHistory();
  const userData = React.useContext(CurrentUserContext);

  function redirectToActive() {
    history.push("/active");
  }

  function redirectToCompleted() {
    history.push("/completed");
  }

  function redirectToSave() {
    history.push("/save");
  }

  return (
    <section className="main">
      <div className="sidebar">
        <div className="sidebar__item" onClick={redirectToActive}>
          <img
            className="sidebar__item_image"
            src={chat_active_icon_white}
            alt="Активный чат"
          />
          <p className="sidebar__item_title">Активные</p>
        </div>
        <div className="sidebar__item" onClick={redirectToCompleted}>
          <img
            className="sidebar__item_image"
            src={chat_inactive_icon_white}
            alt="Не активный чат"
          />
          <p className="sidebar__item_title">Завершенные</p>
        </div>
        <div className="sidebar__item" onClick={redirectToSave}>
          <img
            className="sidebar__item_image"
            src={chat_save_icon_white}
            alt="Сохраненный чат"
          />
          <p className="sidebar__item_title">Сохраненные</p>
        </div>
      </div>
      <div className="main-page">
        <div className="header">
          <p className="header__email">{userData ? userData.email : ""}</p>
          <button
            className="header__button"
            onClick={props.signOut}
            type="button"
          >
            Выход
          </button>
          <p className="header__client"></p>
          <input
            className={`header__search ${
              props.route === "main" ? "header__search_inactive" : ""
            }`}
            placeholder="Поиск"
          />
        </div>
        <div className="main-page__chat-block"></div>
      </div>
    </section>
  );
}

export default Main;
